// Les événements en React
// introduction
function Bienvenue (props){
    return (
        <div>
            <h1>Les événements en React {props.name} </h1>
            <p>
                {props.children}
            </p>
        </div>
    )
}

ReactDOM.render(
    <Bienvenue name="avec Noel">On commence?</Bienvenue>, 
    document.querySelector('#app')
)


class Cours extends React.Component{

    // Ceci est obligatoire si nous voulons utiliser les proprietes (props)
    constructor(props){
        super(props)
    }

    render(props){
        return (
            <div>
                <h1>Les événements en React {this.props.name}</h1>
                <p>
                    {this.props.children}
                </p>
            </div>
        )
    }
}

ReactDOM.render(
    <Cours name="avec Jean">On Débute ?</Cours>,
    document.querySelector('#app1')
)


// Les événements en Reactjs

//exemple 1
class MonManuel extends React.Component{

    constructor(props){
        super(props)
        // Je défini un Etat
        this.state= {n: 0}
    }

    // Ma fonction incrémenter
        incrementer(){
        this.setState({n: this.state.n + 1})
    }

    render (){
        return (
            <div>
                Compteur 1: {this.state.n} <br/>
                <button onClick={this.incrementer.bind(this)}>Incrémenter</button>
            </div>
        )
    }
}


ReactDOM.render(
    <MonManuel></MonManuel>,
    document.querySelector('#app1')
)


// exemple d'un compteur
class Compteur extends React.Component{

    // Mon constructeur
    constructor(props){
        super(props)
        this.state= {n: props.start, timer: null}
        this.toogle= this.toogle.bind(this)
        this.reset= this.reset.bind(this)
    }
    
    // On monte le composant
    componentDidMount(){
        this.play()
    }

    // Je démonte le composant
    componentWillUnmount(){
        window.clearInterval(this.state.timer)
    }
    
    pause(){
        window.clearInterval(this.state.timer)
        this.setState({
            timer: null
        })
    }

    play(){
        window.clearInterval(this.state.timer)
        this.setState({
            timer: window.setInterval(this.Incrementeur.bind(this),1000)
        })
    }

    // Mon incrémenteur
    Incrementeur(){
        this.setState((state, props) => ({n: state.n + props.step}))
    }

    reset(){
        this.pause()
        this.play()
        this.setState((state, props) => ({n: props.start}))
    }

    toogle(){
        return this.state.timer? this.pause() : this.play()
    }

    label(){
        return this.state.timer ? 'En pause' : 'En lecture'
    }

    render() {
        return (
            <div>
                Compteur 2: {this.state.n} <br/>
                    <button onClick= {this.toogle}>{this.label()}</button>
                    <button onClick= {this.reset}>Réinitialiser</button>
            </div>
        )
    }
    
}

Compteur.defaultProps = {
    start: 0,
    step: 1
}

function Principale(){
    return (
        <div>
            <Compteur></Compteur>
        </div>
    )
}

ReactDOM.render(
    <Principale></Principale>,
    document.querySelector('#app2')
)
